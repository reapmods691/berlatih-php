<?php
require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

echo "Realese 0";
echo "<br><br>";

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "Leg : $sheep->legs <br>"; // 2
echo "Cold Blooded : $sheep->cold_blooded <br>"; // false

echo "<br><br>";
echo "Realese 1";
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>"; // "buduk"
echo "Leg :  $kodok->legs <br>"; // 4
echo "Jump : $kodok->jump <br>"; // "hup hop"
echo $kodok->Jump();

echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>"; // "buduk"
echo "Leg :  $sungokong->legs <br>"; // 2
echo "Yell : $sungokong->yell <br>"; // "Auooo"
echo $sungokong->Yell();
?>